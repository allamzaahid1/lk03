/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas pertambahan dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa interface akan lebih cocok digunakan daripada abstract class?
 * "Jawab disini..." 
 * 
 * jawab:
 * interface merupakan sebuah persyaratan yang akan membuat class yang mengimplementasikannya untuk memiliki atribut serta method 
 * yang sama dengan attribut serta method yang telah di deklarasi oleh interface tersebut,
 * berbeda dengan abstract class, satu class dapat mengimplementasikan lebih dari satu interface
 * sehingga class tersebut dapat menggunakan attribut turunan dari beberapa interface yang berbeda
 * 
 * jika class pada kalkulator dikoding untuk memiliki persamaan pada beberapa class
 * serta ada class yang memerlukan attribut lebih dari satu class induk(interface) yang berbeda,
 * interface akan lebih cocok.
 */

import java.util.*;

public class Main {   
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operan1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operan2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        double result = kalkulator.hitung(operan1, operan2);
        System.out.println("Result: " + result);
    }
}