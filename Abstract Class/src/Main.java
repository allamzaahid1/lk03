/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas kalkulator, pertambahan, dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa abstract class akan lebih cocok digunakan daripada interface?
 * "Jawab disini..." 
 * 
 * jawab:
 * abstract class merupakan sebuah class yang akan memaksa setiap class turunannya untuk memiliki atribut atau method 
 * yang sama dengan attribut atau method abstract yang telah di deklarasi oleh abstract class tersebut.
 * walau abstract class dapat diturunkan menjadi banyak class turunan,
 * tetapi satu class turunan hanya dapat meng-extends atau memiliki satu induk abstract class saja
 * sehingga class turunan tidak dapat mengambil atribut dari abstract class lain.
 * 
 * jika kalkulator dikoding agar semua class turunan memiliki persamaan dan/atau dapat menerapkan hal yang mirip, 
 * menggunakan abstract class akan lebih cocok.
 * 
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operand1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operand2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        kalkulator.setOperan(operand1, operand2);
        double result = kalkulator.hitung();
        System.out.println("Hasil: " + result);
    }
}