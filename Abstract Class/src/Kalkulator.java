abstract class Kalkulator {
    double operan1, operan2;
    //Do your magic here...

    //method untuk penjumlahan dan pengurangan dengan abstract class (dengan menyesuaikan nama class dari class main)
    public abstract void setOperan(double operand1, double operand2);
    public abstract double hitung();
}