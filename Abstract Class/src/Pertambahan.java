class Pertambahan extends Kalkulator {
    //Do your magic here...
    double operan1;
    double operan2;

    @Override
    public void setOperan(double operand1, double operand2) {
        this.operan1 = operand1;
        this.operan2 = operand2;
    }

    @Override
    public double hitung() {
        return operan1 + operan2;
    }
}